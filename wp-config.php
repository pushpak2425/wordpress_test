<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('REVISR_GIT_PATH', ''); // Added by Revisr
define('REVISR_WORK_TREE', 'D:\xampp\htdocs\hardik\wordpress-4.9.5\wordpress_test/'); // Added by Revisr
define('DB_NAME', 'wordpress_test');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CXe6w#6[>u}#EYK+y,X(,nCg@K$di1:5eYx_{.K+%^OH 7t?9!d!S<6./RiQkbN~');
define('SECURE_AUTH_KEY',  '=K)ZfS^)qo5UuVWEuz9TJtA_q1wr179q}W=ISmy(hrE}Rnijs)qi.6E=}|JBc}Mg');
define('LOGGED_IN_KEY',    'iuay.0]|T<`o _X>RKzf;U4tynYEVYYY{e*eeW-VlStiAI1}Vz3ob(vMl0 pj&>j');
define('NONCE_KEY',        '7<N#5ch}ld^zMUI!*@%HP/8niJ 7|b%E}4*cgS> <v>rx%@):}0rHlyZo@Y=qcQF');
define('AUTH_SALT',        '8{gko{?V7{_VqgTUzhHAYRF{2McJW6untRpMz#siNS~h`GR]NS2Ll_jVf`_1u7Yv');
define('SECURE_AUTH_SALT', '&W=s8a/#w |uUaYpgiQg|k$z@a+tx7F{[t^wrts=J_h*Op,->p?-#>v-,P#73NzZ');
define('LOGGED_IN_SALT',   'WoO%T2n>nA7Wqq6&Z@uPMeM+9MOx{Z7q+1PRf)? m/8ZHgG5pH=r<<N&8`&?dRgU');
define('NONCE_SALT',       'rn 2*b<8W!b-=${ mmoY4dC-9Oz8DyIp87}xgWuz-zH8]eOx)]#,)x])!*~;R|[o');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
